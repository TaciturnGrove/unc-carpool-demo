UNC Carpool
=================
Carpool System used Internally in FACSS, UNC Chapel Hill


# Setup

Ruby Required

Install Padrino before dev

See app/views/ for the slim template files


```
bundler install
padrino s
```

# Deployment

This app is currently hosted in the UNC Chapel Hill Openshift platform.


# License

MIT License
